package controller;

import entities.Role;
import entities.User;
import interfaces.ClientService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class RegisterController {

    @FXML
    private Button back;

    @FXML
    private Button register;

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    private Text message;

    private ClientService clientService;

    public RegisterController() {
    }

    @FXML
    public void initialize() throws MalformedURLException {
        URL url = new URL("http://localhost:8080/clientService?wsdl");
        QName qName = new QName("http://impl/", "ClientServiceImplService");
        Service service = Service.create(url, qName);
        clientService = service.getPort(ClientService.class);
    }

    public void handleButtonRegister(javafx.event.ActionEvent event) {
        User userToAdd = new User(0, username.getText(), password.getText(), Role.CLIENT);
        clientService.doRegister(userToAdd);
        message.setText("Account successfully created!");
    }

    public void handleButtonBack(javafx.event.ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Login.fxml"));
        Parent root2 = fxmlLoader.load();
        Scene scene = new Scene(root2, 600, 400);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
}
