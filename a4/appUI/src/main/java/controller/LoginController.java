package controller;

import entities.Role;
import entities.User;
import interfaces.ClientService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class LoginController {

    @FXML
    private Button logIn;

    @FXML
    private Button register;

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    private ClientService clientService;

    public LoginController() {
    }

    @FXML
    public void initialize() throws MalformedURLException {
        URL url = new URL("http://localhost:8080/clientService?wsdl");
        QName qName = new QName("http://impl/", "ClientServiceImplService");
        Service service = Service.create(url, qName);
        clientService = service.getPort(ClientService.class);
    }

    public void handleButtonLogIn(javafx.event.ActionEvent event) throws IOException {
        User user = clientService.doLogin(username.getText(), password.getText());
        if (user != null) {
            if (user.getRole() == Role.CLIENT) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Client.fxml"));
                Parent root2 = fxmlLoader.load();

                ClientController clientController = fxmlLoader.getController();
                clientController.setUser(user);

                Scene scene = new Scene(root2, 600, 400);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(scene);
                window.show();
            }
            else {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Admin.fxml"));
                Parent root2 = fxmlLoader.load();
                Scene scene = new Scene(root2, 600, 400);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(scene);
                window.show();
            }
        }
    }

    public void handleButtonRegister(javafx.event.ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Register.fxml"));
        Parent root2 = fxmlLoader.load();
        Scene scene = new Scene(root2, 600, 400);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
}
