package controller;

import dto.Route;
import entities.Package;
import entities.Role;
import entities.RouteEntry;
import entities.User;
import interfaces.ClientService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class ClientController {

    @FXML
    private Button listPack;

    @FXML
    private Button search;

    @FXML
    private Button statusCheck;

    @FXML
    private Button back;

    @FXML
    private TextField packName;

    @FXML
    private TextArea message;

    private ClientService clientService;
    private User user;

    public ClientController() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @FXML
    public void initialize() throws MalformedURLException {
        URL url = new URL("http://localhost:8080/clientService?wsdl");
        QName qName = new QName("http://impl/", "ClientServiceImplService");
        Service service = Service.create(url, qName);
        clientService = service.getPort(ClientService.class);
    }

    public void handleButtonBack(javafx.event.ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Login.fxml"));
        Parent root2 = fxmlLoader.load();
        Scene scene = new Scene(root2, 600, 400);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    public void handleButtonSearch(javafx.event.ActionEvent event) {
        Package pack = clientService.searchPackage(packName.getText());
        if (pack != null) {
            message.setText(pack.toString());
        }
        else {
            message.setText("The package cannot be found!");
        }
    }

    public void handleButtonList(javafx.event.ActionEvent event) {
        List<Package> list = clientService.listPackages(user);
        if (list != null) {
            for (Package p : list) {
                message.setText(p.toString());
            }
        }
        else {
            message.setText("No package found for this username!");
        }
    }

    public void handleButtonCheck(javafx.event.ActionEvent event) {
        Package pack = clientService.findPackage(packName.getText());
        List<Route> list = clientService.checkStatus(pack);
        for (Route r: list) {
            message.appendText("City: " + r.getCity() + " Time: " + r.getTime() + "\n");
        }
    }
}
