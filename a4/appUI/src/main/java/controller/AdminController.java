package controller;

import entities.City;
import entities.Package;
import entities.RouteEntry;
import entities.User;
import interfaces.AdminService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import tornadofx.control.DateTimePicker;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

public class AdminController {

    @FXML
    private Button back;

    @FXML
    private Button remove;

    @FXML
    private Button add;

    @FXML
    private Button update;

    @FXML
    private TextField packName;

    @FXML
    private TextArea description;

    @FXML
    private DateTimePicker routeTime;

    @FXML
    private Text message;

    @FXML
    private ComboBox<User> comboSender;

    @FXML
    private ComboBox<User> comboReceiver;

    @FXML
    private ComboBox<City> comboSendCity;

    @FXML
    private ComboBox<City> comboDestCity;

    @FXML
    private ComboBox<City> comboRouteCity;

    @FXML
    private ComboBox<String> comboPackage;

    @FXML
    private Button tracking;

    private AdminService adminService;

    public AdminController() {
    }

    @FXML
    public void initialize() throws MalformedURLException {
        URL url = new URL("http://localhost:8081/adminService?wsdl");
        QName qName = new QName("http://impl/", "AdminServiceImplService");
        Service service = Service.create(url, qName);
        adminService = service.getPort(AdminService.class);

        ObservableList<City> cities = FXCollections.observableArrayList(adminService.findCities());
        comboSendCity.setItems(cities);
        comboDestCity.setItems(cities);
        comboRouteCity.setItems(cities);

        ObservableList<User> users = FXCollections.observableArrayList( adminService.findUsers());
        comboSender.setItems(users);
        comboReceiver.setItems(users);

        List<String> p = adminService.findPackages().stream().map(Package::getName).collect(Collectors.toList());
        ObservableList<String> pack = FXCollections.observableArrayList(p);
        comboPackage.setItems(pack);
    }

    public void handleButtonBack(javafx.event.ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Login.fxml"));
        Parent root2 = fxmlLoader.load();
        Scene scene = new Scene(root2, 600, 400);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    public void handleButtonAdd(javafx.event.ActionEvent event) {
        User sender = comboSender.getSelectionModel().getSelectedItem();
        User receiver = comboReceiver.getSelectionModel().getSelectedItem();
        City sendCity = comboSendCity.getSelectionModel().getSelectedItem();
        City destCity = comboDestCity.getSelectionModel().getSelectedItem();
        Package pack = new Package(0, sender, receiver, packName.getText(), description.getText(), sendCity, destCity, false);
        adminService.addPackage(pack);
        message.setText("Package was successfully added!");
        comboPackage.getItems().add(packName.getText());
    }

    public void handleButtonRemove(javafx.event.ActionEvent event) {
        String packToRemove = comboPackage.getSelectionModel().getSelectedItem();
        adminService.deletePackage(packToRemove);
        message.setText("Package was deleted");
        comboPackage.getItems().remove(packToRemove);
    }

    public void handleButtonUpdate(javafx.event.ActionEvent event) {
        String packName = comboPackage.getSelectionModel().getSelectedItem();
        Package pack = adminService.findPackage(packName);
        if (!pack.isTracking()) {
            message.setText("Tracking for this package is not enabled");
        }
        else {
            Timestamp time = Timestamp.valueOf(routeTime.getDateTimeValue());
            adminService.addRoute(time.toString(), pack, comboRouteCity.getSelectionModel().getSelectedItem());
            message.setText("Route<City, Time> added!");
        }
    }

    public void handleButtonTracking(javafx.event.ActionEvent event) {
        String packName = comboPackage.getSelectionModel().getSelectedItem();
        Package pack = adminService.findPackage(packName);
        if (pack.isTracking()) {
            message.setText("Package is already tracked");
        }
        else {
            pack.setTracking(true);
            adminService.updatePackage(pack);
            message.setText("Package is now tracked");
        }
    }
}
