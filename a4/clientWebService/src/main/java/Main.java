import dao.PackageDAO;
import dao.RouteDAO;
import dao.UserDAO;
import impl.ClientServiceImpl;

import javax.xml.ws.Endpoint;

public class Main {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8080/clientService", new ClientServiceImpl(new PackageDAO(), new RouteDAO(), new UserDAO()));
    }
}
