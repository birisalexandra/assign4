package impl;

import dao.PackageDAO;
import dao.RouteDAO;
import dao.UserDAO;
import dto.Route;
import entities.Package;
import entities.RouteEntry;
import entities.User;
import interfaces.ClientService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService(endpointInterface = "interfaces.ClientService")
public class ClientServiceImpl implements ClientService {

    private PackageDAO packageDAO;
    private RouteDAO routeDAO;
    private UserDAO userDAO;

    public ClientServiceImpl() {
    }

    public ClientServiceImpl(PackageDAO packageDAO, RouteDAO routeDAO, UserDAO userDAO) {
        this.packageDAO = packageDAO;
        this.routeDAO = routeDAO;
        this.userDAO = userDAO;
    }

    @WebMethod
    public List<Package> listPackages(User user) {
        return packageDAO.findPackages(user);
    }

    @WebMethod
    public Package searchPackage(String name) {
        return packageDAO.findPackage(name);
    }

    @WebMethod
    public List<Route> checkStatus(Package pack) {
        List<Route> list = routeDAO.checkStatus(pack).stream().map(r -> new Route(r.getTime().toString(), r.getCity())).collect(Collectors.toList());
        return list;
    }

    @WebMethod
    public User doLogin(String username, String password) {
        return userDAO.findUser(username, password);
    }

    @WebMethod
    public User doRegister(User user) {
        return userDAO.addUser(user);
    }

    public Package findPackage(String name) {
        return packageDAO.findPackage(name);
    }
}
