import dao.CityDAO;
import dao.PackageDAO;
import dao.RouteDAO;
import dao.UserDAO;
import impl.AdminServiceImpl;

import javax.xml.ws.Endpoint;

public class Main {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8081/adminService", new AdminServiceImpl(new UserDAO(), new CityDAO(), new PackageDAO(), new RouteDAO()));
    }
}
