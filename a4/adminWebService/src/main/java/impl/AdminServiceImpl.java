package impl;

import dao.CityDAO;
import dao.PackageDAO;
import dao.RouteDAO;
import dao.UserDAO;
import entities.City;
import entities.Package;
import entities.RouteEntry;
import entities.User;
import interfaces.AdminService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.sql.Timestamp;
import java.util.List;

@WebService(endpointInterface = "interfaces.AdminService")
public class AdminServiceImpl implements AdminService {

    private UserDAO userDAO;
    private CityDAO cityDAO;
    private PackageDAO packageDAO;
    private RouteDAO routeDAO;

    public AdminServiceImpl() {
    }

    public AdminServiceImpl(UserDAO userDAO, CityDAO cityDAO, PackageDAO packageDAO, RouteDAO routeDAO) {
        this.userDAO = userDAO;
        this.cityDAO = cityDAO;
        this.packageDAO = packageDAO;
        this.routeDAO = routeDAO;
    }

    @WebMethod
    public List<User> findUsers() {
        return userDAO.findUsers();
    }

    @WebMethod
    public List<City> findCities() {
        return cityDAO.findCities();
    }

    @WebMethod
    public List<Package> findPackages() {
        return packageDAO.listPackages();
    }

    @WebMethod
    public Package addPackage(Package pack) {
        return packageDAO.addPackage(pack);
    }

    @WebMethod
    public void deletePackage(String name) {
        packageDAO.deletePackage(name);
    }

    public void addRoute(String time, Package pack, City city) {
        Timestamp timestamp = Timestamp.valueOf(time);
        RouteEntry routeEntry = new RouteEntry(0, timestamp, pack, city);
        routeDAO.addRouteEntry(routeEntry);
    }

    public Package findPackage(String name) {
        return packageDAO.findPackage(name);
    }

    public void updatePackage(Package pack) {
        packageDAO.updatePackage(pack);
    }
}
