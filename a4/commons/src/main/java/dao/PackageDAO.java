package dao;

import entities.User;
import entities.Package;
import helpers.HibernateConfig;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

import static jdk.nashorn.internal.objects.NativeError.printStackTrace;

public class PackageDAO {

    public Package addPackage(Package pack) {
        int packId = -1;
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            packId = (Integer) session.save(pack);
            pack.setPackId(packId);
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return pack;
    }

    @SuppressWarnings("unchecked")
    public List<Package> findPackages(User user) {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;
        List<Package> packages = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Package WHERE user_id_receive = :id");
            query.setParameter("id", user.getUserId());
            packages = query.list();
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return packages;
    }

    public Package findPackage(String name) {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;
        Package pack = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Package WHERE name = :n");
            query.setParameter("n", name);
            pack = (Package) query.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return pack;
    }

    public List<Package> listPackages() {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;
        List<Package> packages = null;

        try {
            tx = session.beginTransaction();
            packages = session.createQuery("FROM Package").list();
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return packages;
    }

    public void deletePackage(String name)
    {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("delete FROM Package WHERE name = :name");
            query.setParameter("name", name);
            query.executeUpdate();
            tx.commit();
        }  catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
    }

    public void updatePackage(Package pack)
    {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            Package persisted = findPackage(pack.getName());
            persisted.setTracking(pack.isTracking());
            session.update(persisted);
            tx.commit();
        }  catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
    }

}
