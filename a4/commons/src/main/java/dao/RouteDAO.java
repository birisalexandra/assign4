package dao;

import entities.RouteEntry;
import entities.Package;
import helpers.HibernateConfig;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

import static jdk.nashorn.internal.objects.NativeError.printStackTrace;

public class RouteDAO {

    public RouteEntry addRouteEntry(RouteEntry routeEntry) {
        int routeId = -1;
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            routeId = (Integer) session.save(routeEntry);
            routeEntry.setRouteId(routeId);
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return routeEntry;
    }

    @SuppressWarnings("unchecked")
    public List<RouteEntry> checkStatus(Package pack) {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;
        List<RouteEntry> entries = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM RouteEntry WHERE pack_id = :id");
            query.setParameter("id", pack.getPackId());
            entries = query.list();
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return entries;
    }
}
