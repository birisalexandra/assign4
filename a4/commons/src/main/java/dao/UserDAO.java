package dao;

import entities.User;
import helpers.HibernateConfig;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

import static jdk.nashorn.internal.objects.NativeError.printStackTrace;

public class UserDAO {

    public User findUser(String username, String password) {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;
        User user = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :u AND password = :p");
            query.setString("u", username);
            query.setString("p", password);
            user = (User) query.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return user;
    }

    public User addUser(User user) {
        int userId = -1;
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            userId = (Integer) session.save(user);
            user.setUserId(userId);
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    public List<User> findUsers() {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;
        List<User> users = null;

        try {
            tx = session.beginTransaction();
            users = session.createQuery("FROM User").list();
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return users;
    }
}
