package dao;

import entities.City;
import helpers.HibernateConfig;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

import static jdk.nashorn.internal.objects.NativeError.printStackTrace;

public class CityDAO {

    @SuppressWarnings("unchecked")
    public List<City> findCities() {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;
        List<City> cities = null;

        try {
            tx = session.beginTransaction();
            cities = session.createQuery("FROM City").list();
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return cities;
    }
}
