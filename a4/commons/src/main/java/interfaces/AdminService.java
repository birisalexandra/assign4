package interfaces;

import entities.City;
import entities.Package;
import entities.RouteEntry;
import entities.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface AdminService {

    @WebMethod
    List<User> findUsers();

    @WebMethod
    List<City> findCities();

    @WebMethod
    List<Package> findPackages();

    @WebMethod
    Package addPackage(Package pack);

    @WebMethod
    void deletePackage(String name);

    @WebMethod
    void addRoute(String time, Package pack, City city);

    @WebMethod
    Package findPackage(String name);

    @WebMethod
    void updatePackage(Package pack);
}
