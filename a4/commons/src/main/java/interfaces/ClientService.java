package interfaces;

import dto.Route;
import entities.Package;
import entities.RouteEntry;
import entities.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ClientService {

    @WebMethod
    List<Package> listPackages(User user);

    @WebMethod
    Package searchPackage(String name);

    @WebMethod
    List<Route> checkStatus(Package pack);

    @WebMethod
    User doLogin(String username, String password);

    @WebMethod
    User doRegister(User user);

    @WebMethod
    Package findPackage(String name);
}
