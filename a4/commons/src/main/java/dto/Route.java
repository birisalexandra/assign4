package dto;

import entities.City;

public class Route {

    private String time;
    private City city;

    public Route() {
    }

    public Route(String time, City city) {
        this.time = time;
        this.city = city;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
