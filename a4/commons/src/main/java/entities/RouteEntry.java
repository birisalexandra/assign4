package entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="routeentry")
public class RouteEntry {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "route_id")
    private Integer routeId;

    @Column(name = "time")
    private Timestamp time;

    @ManyToOne
    @JoinColumn(name = "pack_id")
    private Package trackedPackage;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    public RouteEntry() {
    }

    public RouteEntry(Integer id, Timestamp time, Package trackedPackage, City city) {
        this.routeId = id;
        this.time = time;
        this.trackedPackage = trackedPackage;
        this.city = city;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public Package getTrackedPackage() {
        return trackedPackage;
    }

    public void setTrackedPackage(Package trackedPackage) {
        this.trackedPackage = trackedPackage;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
