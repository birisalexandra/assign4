package entities;

import javax.persistence.*;

@Entity
@Table(name="package")
public class Package {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "pack_id")
    private Integer packId;

    @ManyToOne
    @JoinColumn(name = "user_id_send")
    private User sender;

    @ManyToOne
    @JoinColumn(name = "user_id_receive")
    private User receiver;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "city_id_send")
    private City sendCity;

    @ManyToOne
    @JoinColumn(name = "city_id_dest")
    private City destinationCity;

    @Column(name = "tracking")
    private boolean tracking;

    public Package() {
    }

    public Package(Integer packId, User sender, User receiver, String name, String description, City sendCity, City destinationCity, boolean tracking) {
        this.packId = packId;
        this.sender = sender;
        this.receiver = receiver;
        this.name = name;
        this.description = description;
        this.sendCity = sendCity;
        this.destinationCity = destinationCity;
        this.tracking = tracking;
    }

    public Integer getPackId() {
        return packId;
    }

    public void setPackId(Integer packId) {
        this.packId = packId;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public City getSendCity() {
        return sendCity;
    }

    public void setSendCity(City sendCity) {
        this.sendCity = sendCity;
    }

    public City getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(City destinationCity) {
        this.destinationCity = destinationCity;
    }

    public boolean isTracking() {
        return tracking;
    }

    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }

    @Override
    public String toString() {
        return "Package{" +
                "packId=" + packId +
                ", sender=" + sender +
                ", receiver=" + receiver +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", sendCity=" + sendCity +
                ", destinationCity=" + destinationCity +
                ", tracking=" + tracking +
                '}';
    }
}
